select
cast(admin_activity.createtime as signed) as createtime,
cast(admin_activity.record as signed) as record_id,
cast(login as char) as user_name,
case
when address = '' then cast(concat('https://',(select host from server_status order by marker desc limit 1)) as char)
when address like '%::1%' then cast(replace(address, '[::1]', (select host from server_status order by marker desc limit 1)) as char)
else cast(address as char)
end as src_host,
case
when subsys = 1 then 'Control Center'
when subsys = 2 then 'WebAPI'
when subsys = 3 then 'Server'
when subsys = 0 then 'Utilities'
else cast(subsys as char)
end as subsystem,
cast(oper as signed) as action,
case
when status = 1 then 'success'
when status = 2 then 'failure'
when status = 3 then 'ongoing'
when status = 5 then 'delayed'
else cast(status as char)
end as status,
cast(ad.data as char) as data
from
admin_activity
left join (
select distinct
record,
concat(' "', item, '"="', value, '"') as data
from
activity_data ad
group by record
) as ad on (ad.record = admin_activity.record)
WHERE
(admin_activity.createtime > :createtime)
ORDER BY admin_activity.createtime;